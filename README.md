Hamster-Hoarder
===============

A simple script to scrape xhamster user videos


To get it to run you will need the following.

1. youtube-dl (http://rg3.github.io/youtube-dl/)   *****MAKE SURE YOU ARE RUNNING THE LATEST VERSION*****
2. The username for the account you want to scrape (This can be found in the URL for the users profile page)
3. How many pages of videos you want to download.

Once you have everything make hamhor.sh Executable and run it.

It will ask you some questions and start downloading.
